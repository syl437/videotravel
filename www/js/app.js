// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform,$rootScope) {
  $ionicPlatform.ready(function() {
	  
	$rootScope.HostUrl = 'http://tapper.co.il/videotravel/';  
	$rootScope.mediaFile = '';
	$rootScope.showUploadBtn = true;
	
	$rootScope.videoCapturePlusDemo = function()
	{
		navigator.device.capture.captureVideo(captureSuccess, captureError, { limit: 1, duration: 5,  quality: 0 });
	}
	

		function captureSuccess(mediaFiles) {
			$rootScope.showUploadBtn = true;
			$rootScope.mediaFile = mediaFiles;
			
		  var i, len;
		  for (i = 0, len = mediaFiles.length; i < len; i++) {
			var mediaFile = mediaFiles[i];
			//uploadFile(mediaFiles[i]);

			mediaFile.getFormatData(getFormatDataSuccess, getFormatDataError);

			var vid = document.createElement('video');
			vid.id = "theVideo";
			vid.width = "240";
			vid.height = "160";
			vid.controls = "controls";
			var source_vid = document.createElement('source');
			source_vid.id = "theSource";
			source_vid.src = mediaFile.fullPath;
			vid.appendChild(source_vid);
			document.getElementById('video_container').innerHTML = '';
			document.getElementById('video_container').appendChild(vid);
			document.getElementById('video_meta_container2').innerHTML = parseInt(mediaFile.size / 1000) + 'KB ' + mediaFile.type;
		  }
		}
		
		$rootScope.uploadVideo = function()
		{
			var mediaFiles = $rootScope.mediaFile;
			
			for (i = 0, len = mediaFiles.length; i < len; i++) 
			{
				uploadFile(mediaFiles[i]);
			}
		}
		
		
		
		function uploadFile(mediaFile) {
			alert ("uploading to tapper/videotravel please wait...")
			var myImg = mediaFile;
			var options = new FileUploadOptions();
			options.mimeType = 'mp4';
			options.fileKey = "file";
			options.fileName = mediaFile.name; // myImg.substr(myImg.lastIndexOf('/') + 1);

			
			var ft = new FileTransfer(),
				path = mediaFile.fullPath,
				name = mediaFile.name;
			/*
			ft.upload(path,
				$rootScope.HostUrl+'/UploadVideo.php',
				function(result) {
					alert ("ok1")
					console.log('Upload success: ' + result.responseCode);
					console.log(result.bytesSent + ' bytes sent');
				},
				function(error) {
					alert ("bad1")
					console.log('Error uploading file ' + path + ': ' + error.code);
				},
				{ fileName: name });
			*/
			ft.upload(path, encodeURI($rootScope.HostUrl+'/UploadVideo.php'), onUploadSuccess, onUploadFail, options);

		}
		
		function onUploadSuccess()
		{
			alert ("done");
			$rootScope.showUploadBtn = false;
		}

		function onUploadFail(data)
		{
			alert("onUploadFail : " + data);
			$rootScope.showUploadBtn = false;
		}
	

		function getFormatDataSuccess(mediaFileData) {
		  document.getElementById('video_meta_container').innerHTML = mediaFileData.duration + ' seconds, ' + mediaFileData.width + ' x ' + mediaFileData.height;
		}

		function captureError(error) {
		  // code 3 = cancel by user
		  alert('Returncode: ' + JSON.stringify(error.code));
		}

		function getFormatDataError(error) {
		  alert('A Format Data Error occurred during getFormatData: ' + error.code);
		}


		
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});
